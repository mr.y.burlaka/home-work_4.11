#include <iostream>
#include <istream>
#include <string>
#include <vector>
#include <list>

//
//State Pattern
//

class Character;
class EMovementState;
class RunningState;
class WalkingState;
class TiredState;

class Character
{
    std::string name;
public:
    Character(const std::string& name) : name(name) {   }
    std::string GetName()
    {
        return name;
    }
    virtual void ToRun(EMovementState*) = 0;
    virtual void Walking(EMovementState*) = 0;
    virtual void Tired(EMovementState*) = 0;
};

class EMovementState
{
private:
    Character* state;
public:
    EMovementState(Character* state) : state(state) {  }

    void ToRun()
    {
        std::cout << "From " << state->GetName() <<" to Run " << std::endl;
        state->ToRun(this);
    }
    void Walking()
    {
        std::cout << "From " << state->GetName() <<" to Walk " << std::endl;
        state->Walking(this);
    }
    void Tired()
    {
        std::cout << "From " << state->GetName() <<" to Tired " << std::endl;
        state->Tired(this);
    }

    void SetState(Character* person)
    {
        std::cout << "A person switches from " << state->GetName()
            << " to " << person->GetName() << std::endl;
        delete state;
        state = person;
    }
    Character* GetState()
    {
        return state;
    }

    ~EMovementState()
    {
        delete state;
    }
};



class WalkingState : public Character
{
public:
    WalkingState() : Character("Walk") {}
    virtual void ToRun(EMovementState* state);
    virtual void Walking(EMovementState* state);
    virtual void Tired(EMovementState* state);
};
class RunningState : public Character
{
public:
    RunningState() : Character("Run") {}
    virtual void ToRun(EMovementState* state);
    virtual void Walking(EMovementState* state);
    virtual void Tired(EMovementState* state);
};
class TiredState : public Character
{
public:
    TiredState() : Character("Tired") {}
    virtual void ToRun(EMovementState* state);
    virtual void Walking(EMovementState* state);
    virtual void Tired(EMovementState* state);
};
/////////////////////////////////////////////////////////
void RunningState::Walking(EMovementState* state)
{
    state->SetState(new WalkingState());
}
void RunningState::ToRun(EMovementState* state)
{
    state->SetState(new TiredState());
}
void RunningState::Tired(EMovementState* state)
{
    std::cout << "The man fell \n ";
}
/////////////////////////////////////////////////////////
void WalkingState::ToRun(EMovementState* state)
{
    state->SetState(new RunningState());
}
void WalkingState::Walking(EMovementState* state)
{
    std::cout << "The man keeps walking\n";
}
void WalkingState::Tired(EMovementState* state)
{
    state->SetState(new TiredState());
}
/////////////////////////////////////////////////////////
void TiredState::Tired(EMovementState* state)
{
    std::cout << "The character is still tired" << std::endl;
}
void TiredState::Walking(EMovementState* state)
{
    std::cout << "The man has rested and starts walking" << std::endl;
}
void TiredState::ToRun(EMovementState* state)
{
    std::cout << "The man fell";
}


//
//Visitor Patter
//

class Slot_1;
class Slot_2;
class Slot_3;

class Visitor
{
public:
    virtual void visit(Slot_1& ref) = 0;
    virtual void visit(Slot_2& ref) = 0;
    virtual void visit(Slot_3& ref) = 0;
    virtual ~Visitor() = default;
};

class Element 
{
public:
    virtual void accept(Visitor& v) = 0;
    virtual ~Element() = default;
};

class Slot_1 : public Element 
{
public:
    void accept(Visitor& v) override 
    {
        v.visit(*this);
    }
};

class Slot_2 : public Element 
{
public:
    void accept(Visitor& v) override 
    {
        v.visit(*this);
    }
};

class Slot_3 : public Element 
{
public:
    void accept(Visitor& v) override 
    {
        v.visit(*this);
    }
};

class GetType : public Visitor 
{
public:
    std::string value;

public:
    void visit(Slot_1& ref) override 
    {
        value = "Slot_1 skill ... ";
    }
    void visit(Slot_2& ref) override 
    {
        value = "Slot_2 skill ... ";
    }
    void visit(Slot_3& ref) override 
    {
        value = "Slot_3 skill ... ";
    }
};

//
//Observer Pattern
//


class Observer 
{
public:
	virtual void update(int infantry, int heavyMachinery, int lightEquipment) = 0;
};

class MilitaryUnit 
{
private:
	int infantry;
	int heavyMachinery;
	int lightEquipment;
	std::vector<Observer*> observers;

public:
	void registerObserver(Observer* observer) 
	{
		observers.push_back(observer);
	}
	void notifyObservers() 
	{
		for (Observer* observer : observers) 
		{
			observer->update(infantry, heavyMachinery, lightEquipment);
		}
	}

	void setArmyChanges(int soldier, int heavyMachin, int ligEquip) 
	{
		infantry = soldier;
		heavyMachinery = heavyMachin;
		lightEquipment = ligEquip;
		notifyObservers();
	}
};

class Display : public Observer 
{
public:
	void update(int infantry, int heavyMachinery, int lightEquipment) 
	{
		std::cout << "Military personnel: Infantry = " << infantry
			<< ", Heavy Machinery = " << heavyMachinery
			<< ", Light Equipment = " << lightEquipment
			<< std::endl;
	}
};

//
//Chain of responsibility Pattern
//

class CriminalAction 
{
    friend class HackingTool;
    int complexity;
    const char* description;
public:
    CriminalAction(int complexity, const char* description) 
        : complexity(complexity), description(description) {}
};

class HackingTool 
{
protected:
    int Hacking;
    HackingTool* nextTool;
    virtual void hackingResults(const char* description) {}    // собственно расследование
public:
    HackingTool(int Hacking) : Hacking(Hacking), nextTool(nullptr) {}
    virtual ~HackingTool() 
    {
        delete nextTool;
    }
    HackingTool* setNext(HackingTool* nextTool_2) 
    {
        nextTool = nextTool_2;
        return nextTool;
    }
    void hackingAttempt(CriminalAction* criminalAction) {
        if (Hacking < criminalAction->complexity) 
        {
            if (nextTool) 
            {
                nextTool->hackingAttempt(criminalAction);
            }
            else 
            {
                std::cout << "Don't mess with this tool" << std::endl;
            }
        }
        else 
        {
            hackingResults(criminalAction->description);
        }
    }
};

class Tools_1_Level : public HackingTool 
{
protected:
    void hackingResults(const char* description) 
    {
        std::cout << "The lock is weak \"" << description << "\" and a crowbar will do!" << std::endl;
    }

public:

    Tools_1_Level(int Hacking) : HackingTool(Hacking) {}
};

class Tools_2_Level : public HackingTool 
{
protected:
    void hackingResults(const char* description) 
    {
        std::cout << "The lock is complicated\"" << description << "\" you need a master key!!" << std::endl;
    }

public:
    Tools_2_Level(int Hacking) : HackingTool(Hacking) {}
};

class Tools_3_Level : public HackingTool 
{
protected:
    void hackingResults(const char* description) 
    {
        std::cout << "The castle of the extra level \"" << description << "\" needs everything there is!!!" << std::endl;
    }

public:
    Tools_3_Level(int Hacking) : HackingTool(Hacking) {}
};

//
//Command Pattern
//

class Command
{
public:
    virtual void execute() = 0;
};

class Weapon
{
private:
    std::string weaponName;
public:
    Weapon(const std::string& n) : weaponName(n) {	}

    void openFire()
    {
        std::cout << "The " << weaponName << " is firing" << std::endl;
    }

    void startReload()
    {
        std::cout << " Start reloading " << weaponName << std::endl;
    }
};

class CommandOpenFire : public Command
{
private:
    Weapon& weapons;

public:
    CommandOpenFire(Weapon& weapon) : weapons(weapon) {	}

    void execute() { weapons.openFire(); }
};

class CommandStartReloading : public Command
{
private:
    Weapon& weapons;

public:
    CommandStartReloading(Weapon& weapon) : weapons(weapon) {	}

    void execute() { weapons.startReload(); }
};

class Control
{
private:
    std::vector<Command*> commands;
public:
    void addCommand(Command* cmd)
    {
        commands.push_back(cmd);
    }

    void pressButton(int slot)
    {
        if (slot >= 0 && slot < commands.size())
        {
            commands[slot]->execute();
        }
    }
};

int main() 
{
    std::cout << "State Pattern\n";
    std::cout << std::endl;
    EMovementState* Ivan = new EMovementState(new WalkingState());

    Ivan->Walking();
    Ivan->ToRun();
    Ivan->ToRun();
    Ivan->ToRun();
    Ivan->Tired();
    Ivan->Walking();
    delete Ivan;
    std::cout << std::endl;

    std::cout << "Visitor Patter\n";
    std::cout << std::endl;
    Slot_1 slot1;
    Slot_2 slot2;
    Slot_3 slot3;
    Element* elements[] = { &slot1, &slot2, &slot3 };
    for (auto elem : elements)
    {
        GetType visitor;
        elem->accept(visitor);
        std::cout << visitor.value << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Observer Pattern\n";
    std::cout << std::endl;
    MilitaryUnit militaryUnit;

    Display display1;
    Display display2;

    militaryUnit.registerObserver(&display1);
    militaryUnit.registerObserver(&display2);

    militaryUnit.setArmyChanges(1500, 100, 250);
    militaryUnit.setArmyChanges(1100, 90, 170);
    std::cout << std::endl;


    std::cout << "Chain of responsibility Pattern\n";
    std::cout << std::endl;

    std::cout << "Hacking results:" << std::endl;
    HackingTool* policeman = new Tools_1_Level(1);
    policeman
        ->setNext(new Tools_2_Level(2))
        ->setNext(new Tools_3_Level(3));

    policeman->hackingAttempt(new CriminalAction(1, "Padlock"));
    policeman->hackingAttempt(new CriminalAction(3, "Electric lock with biometrics"));
    policeman->hackingAttempt(new CriminalAction(2, "Safe with password"));
    std::cout << std::endl;

    std::cout << "Command Pattern\n";
    std::cout << std::endl;

    // Create weapon
    Weapon pistol("Pistol");

    // Create commands weapon
    CommandOpenFire OpenFire(pistol);
    CommandStartReloading StartReloading(pistol);

    Control control;

    // Set commands for control buttons
    int buttons;
    std::cout << "What to do: \n 1.Open Fire for weapon \n 2.Start reloading weapon" << std::endl;
    std::cin >> buttons;
    if (buttons == 1)
    {
        control.addCommand(&OpenFire);
        control.pressButton(0);
        return 0;
    }
    if (buttons == 2)
    {
        control.addCommand(&StartReloading);
        control.pressButton(0);
        return 0;
    }
    else
    {
        std::cout << " WTF ??? ";
    }
}
